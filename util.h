#ifndef UTIL_
#define UTIL_

#include "includes/SDL2/SDL.h"
#include "includes/SDL2/SDL_image.h"
#include <omp.h>

#define DEBUG 0

#define IDLE_TIME 100
#define SET_OpenCR_THREADS_NUM(n) (omp_set_num_threads(n))

#define SEED 4711

typedef char bool;
#define true 1
#define false 0

#endif
