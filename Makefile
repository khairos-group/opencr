CC = gcc
CPPFLAGS= `pkg-config --cflags sdl2`
CFLAGS = -Wall -Wextra -Werror -std=c99 -O3 -march=corei7
SRCS = main.c adaptive_thresholding.c graphics.c median_filter.c character_detection.c network.c open_cr.c detect_rotation.c graphic_interface.c union_find.c convo.c
PROG = test
LDLIBS= `pkg-config --libs sdl2`

OPENMP = -fopenmp -lm
SDL = -lSDL2
SDL_IMAGE = -lSDL2_image
GTK = `pkg-config --cflags --libs gtk+-3.0`
GTK_LIBS = `pkg-config --libs gtk+-3.0`

LIBS = $(SDL) $(OPENMP) $(LD_LIBS) $(SDL_IMAGE)

$(PROG):$(SRCS)
	$(CC) $(SRCS) $(CFLAGS) $(CPPFLAGS) $(GTK) -o $(PROG) $(LIBS)

clean:
	rm -rf *.o
	rm -rf test
