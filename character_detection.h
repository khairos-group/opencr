#ifndef CHARACTER_DETECTION_
#define CHARACTER_DETECTION_

#include "util.h"
#include "graphics.h"
#include "union_find.h"

void DepthFirstSearch(SDL_Surface* image, unsigned int* pixels, int x, int y,
                            int* i, int currentLabel, LABELED_POINT* labels);
int FindComponents(SDL_Surface* image, unsigned int* pixels,
                    LABELED_POINT* labels);
void DetectBlockFromACertainXY(SDL_Surface* image, unsigned int* pixels,
                                RECTANGLE* lines, int X, int Y, int* linesNB);

RECTANGLE DetectLineFromACertainXY(SDL_Surface* image, unsigned int* pixels ,
                                   int X, int Y);

RECTANGLE DetectCharacterFromACertainLineAndX(SDL_Surface* image,
        unsigned int* pixels,
        RECTANGLE line, int X);


void RLSA(SDL_Surface* image, unsigned int* pixels, int threshold);

#endif
