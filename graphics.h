#ifndef GRAPHICS_
#define GRAPHICS_

#include "util.h"

#define WINDOW_NAME "OpenCR"

#if SDL_BYTEORDER == SDL_BIG_ENDIAN

    #define MASK_RED 0xff000000
    #define MASK_GREEN  0x00ff0000
    #define MASK_BLUE 0x0000ff00
    #define MASK_ALPHA 0x000000ff

#else

    #define MASK_RED 0x000000ff
    #define MASK_GREEN  0x0000ff00
    #define MASK_BLUE  0x00ff0000
    #define MASK_ALPHA 0xff000000

#endif

#define BLACK 0x00000000
#define WHITE 0xFFFFFFFF

typedef struct PIXEL
{
    SDL_Color color;
    int X, Y;
}PIXEL;

typedef struct POINT
{
    int X, Y;
}POINT;

typedef struct LABELED_POINT
{
    int label;
    int X, Y;
}LABELED_POINT;

typedef struct RECTANGLE
{
    POINT TL, BR;
}RECTANGLE;

typedef struct PIXEL_AND_INTENSITY
{
    unsigned int color;
    unsigned int intensity;
}PIXEL_AND_INTENSITY;


unsigned char GetPixelComp32(unsigned int* pixels, int x, int y, int c);
void PutPixelComp32(unsigned int* pixels, int x, int y, int c, unsigned char val);
void Set_Pixel(SDL_Surface* image, PIXEL pixel, unsigned int* pixels);
void DrawPixels(unsigned int* pixels, int pitch, SDL_Texture* pixelsTexture,
 SDL_Renderer* renderer);
Uint32 Get_Pixel(SDL_Surface* image,unsigned int* pixels, int x, int y);
void Resize(SDL_Surface* image, unsigned int* pixels, RECTANGLE* rect,
             unsigned int* dest);
unsigned int* ResizePixels(unsigned int pixels[], int w1, int h1, int w2, int h2);
void Stretch_Nearest(unsigned int* src, unsigned int* dest, int w1, int h1, int w2, int h2);

#endif
