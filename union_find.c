#include "union_find.h"

long Find(long* labels, long pos)
{
	while(labels[pos]!=pos) pos=labels[pos];
	return pos;
}

long Union(long* labels, long root0, long root1)
{
	if (root0==root1) return root0;
	if (root0==NO_ROOT) return root1;
	if (root1==NO_ROOT) return root0;
	if (root0<root1)
	{
		labels[root1]=root0;
		return root0;
	}
	else
	{
		labels[root0]=root1;
		return root1;
	}
}

long Add(long* labels, long pos, long root)
{
	if (root==NO_ROOT)
		labels[pos]=pos;
	else
		labels[pos]=root;
	return labels[pos];
}

void Label(long* labels,unsigned int* image, int W, int H)
{
	long x,y,root,pos;

	pos=0;
	for(y=0; y<H; y++)
	{
		for(x=0; x<W; x++,pos++)
		{
			root=NO_ROOT;

			if ( (x>0) && (image[pos-1]==image[pos]) )
				root = Union(labels, Find(labels, pos-1) , root);

			if ( (x>0 && y>0) && (image[pos-1-W]==image[pos]) )
				root = Union(labels, Find(labels, pos-1-W) , root);

			if ( (y>0) && (image[pos-W]==image[pos]) )
				root = Union(labels, Find(labels, pos-W) , root);

			if ( (x<(W-1) && y>0) && (image[pos+1-W]==image[pos]) )
				root = Union(labels, Find(labels, pos+1-W) , root);

			root = Add(labels, pos, root );
		}
	}

	for(pos=0; pos<(W*H); pos++)
		labels[pos] = Find(labels, pos);

	long label=0;
	for(pos=0; pos<(W*H); pos++)
		if (labels[pos]==pos)
			labels[pos] = label++;
		else
			labels[pos] = labels[labels[pos]];
}
