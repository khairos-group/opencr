#ifndef NETWORK_
#define NETWORK_

#include "util.h"
#include "graphics.h"

#define PATTERN_X 10
#define PATTERN_Y 17
#define PATTERN_XY (PATTERN_X*PATTERN_Y)
#define CHARSET 52

#define NUMBER_OF_CLUSTERS CHARSET
#define VECTOR_LENGTH PATTERN_XY
#define TRAINING_PATTERNS 3*CHARSET

#define DECAY_RATE 0.96
#define MIN_ALPHA 0.01

/*typedef struct NETWORK
{
        int           Units;
        int*          Output;
        int*          Threshold;
        int**         Weight;
}NETWORK;

typedef struct SIG_TO_CHAR
{
    int signature;
    char character;
}SIG_TO_CHAR;*/

/*float* CouputeNetworkOutPut(float* input, float* output, NETWORK* network);
float* ComputeLayerOutPut(float* input, float* output, int count,
                                LAYER* layer);*/
/*
void InitNetwork(NETWORK* network);
void ComputeWeights(NETWORK* network);
void SetInput(FILE* f, NETWORK* network, int* Input);
void GetOutput(FILE* f, NETWORK* network, int* Output);
void InitApp(char Pattern[CHARSET][PATTERN_Y][PATTERN_X],
            char Pattern_[CHARSET][PATTERN_Y][PATTERN_X]);
void PropagateNetwork(NETWORK* network);
void ComputeNetwork(FILE* f, NETWORK* network, int* Input);
void ComputeEverything(FILE* f, NETWORK* network);
bool PropagateUnit(NETWORK* network, int i);
void WriteNetworkOutPut(FILE* f, NETWORK* network);
void ShutDownNetwork(NETWORK* network);
char Search(long signature);
int Random(int l, int h);*/

void Init_LVQN(int clusterNum, int* trainingPatterns, double** w);
void Training(double alpha, int** patterns, double* d, double** w, int* targets);
int GetCluster(int* inputPattern, double* d, double** w);
void UpdateWeights(int vectorNumber, int alpha, int dMin, double** w, int** patterns, int* targets);
void ComputeInput(int** vectorArray, int vectorNumber, double* d, double** w);
void ComputeInput2(int* vectorArray, double* d, double** w);
void clearArray(double* array);
int Minimum(double* array);
void ComputeNetwork(int charsNb, int patterns_[CHARSET][PATTERN_Y][PATTERN_X],
                    int patterns__[charsNb*5][PATTERN_Y][PATTERN_X], RECTANGLE* chars, const char* outPutFileName);
#endif
