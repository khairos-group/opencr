#include "graphics.h"
#include "median_filter.h"
#include "adaptive_thresholding.h"
#include "detect_rotation.h"
#include "character_detection.h"
#include "network.h"
#include "graphic_interface.h"
#include "convo.h"
#include "median_filter.h"
#include "open_cr.h"
#include <stdio.h>

bool quit;

SDL_Event event;

SDL_Surface* image;
SDL_Surface* charSetImage;
SDL_Surface* formattedImage;
SDL_Window* window;
SDL_Renderer* renderer;
SDL_Texture* texture;
void* pixels;

void Init_OpenCR(int argc, char** argv)
{
	const char* imagePath = NULL;
	const char* charSetPath = NULL;
	const char* outPutFileName = NULL;
	const char* angleChar = NULL;
	bool hough, filters;

	/*if (argc == 3 && argv[1] != '\0' && argv[2] != '\0')
	{
	    rotation = (float)atof(argv[2]);
	    charSetPath = argv[1];
	}
	else if (argc > 0 && argc != 3)
	{
		printf("Usage : \"./test 'image path' 'charset path' "
		       "'rotation degree'\"\n");
		exit(EXIT_FAILURE);
	}*/

	if (argc > 1)
	{
		printf("              ---------------            \n"
		       "                   OpenCR                \n"
		       "               Khairos Group             \n"
		       "              ---------------            \n"
		       "An Optical Character Recognition software\n");
		exit(EXIT_FAILURE);
	}

	quit = false;

	if (SDL_Init(SDL_INIT_VIDEO) == -1)
	{
		printf("SDL: An error occured while loading SDL2...");
		exit(EXIT_FAILURE);
	}

	int flags=IMG_INIT_JPG|IMG_INIT_PNG;
	int initted=IMG_Init(flags);

	if((initted&flags) != flags)
	{
		printf("IMG_Init: Failed to init required jpg and png support!\n");
		printf("IMG_Init: %s\n", IMG_GetError());
		exit(EXIT_FAILURE);
	}

	Init_GTK(argc, argv, &outPutFileName, &angleChar, &charSetPath, &imagePath, &hough, &filters);

	outPutFileName = "RESULT.txt";

	angleChar = angleChar == NULL ? "0.000" : angleChar;

	if (imagePath == NULL && charSetPath == NULL)
	{
		printf("OpenCR: Error while loading image\n");
		exit(EXIT_FAILURE);
	}

	image = IMG_Load(imagePath);
	charSetImage = IMG_Load(charSetPath);

	if(!image ||!charSetImage)
	{
		printf("IMG_Load: %s\n", IMG_GetError());
		exit(EXIT_FAILURE);
	}
	//Changing image format
	formattedImage = SDL_ConvertSurfaceFormat(image, image->format->format, 0);

	window = SDL_CreateWindow(WINDOW_NAME,SDL_WINDOWPOS_UNDEFINED,
	                          SDL_WINDOWPOS_UNDEFINED, image->w, image->h, 0);

	renderer = SDL_CreateRenderer(window, -1, 0);

	texture = SDL_CreateTexture(renderer, image->format->format,
	                            SDL_TEXTUREACCESS_STREAMING, formattedImage->w, formattedImage->h);

	int pitch = 0;

	SDL_LockTexture(texture, &formattedImage->clip_rect, &pixels, &pitch);

	pixels = malloc(image->pitch*image->h*sizeof(unsigned int));
	memcpy(pixels, formattedImage->pixels, (formattedImage->pitch *
	                                        formattedImage->h));

	SDL_UnlockTexture(texture);
	//Successfully changed image format

	float rotation = (float)atof(angleChar);
	Run_OpenCR(rotation, outPutFileName, hough, filters);
}

void Update_OpenCR()
{
	while( SDL_PollEvent( &event ))
		if (event.type == SDL_QUIT)
			exit(EXIT_SUCCESS);

	DrawPixels(pixels, formattedImage->pitch, texture, renderer);

	SDL_Delay(IDLE_TIME);
}

void Run_OpenCR(float rotation, const char* outPutFileName, bool hough, bool filters)
{
	unsigned int* pixels2 = malloc(formattedImage->h*formattedImage->w*
	                               sizeof(unsigned int));
	//rotation+=1;
	Grayscale(formattedImage, pixels);
	if (filters)
		MedianFilter(formattedImage, pixels, 3);

	//MedianFilter(formattedImage, pixels, 3);
	Otsu(image, pixels);
	SimpleBinarize(charSetImage, charSetImage->pixels);
	//Rotation(formattedImage, pixels);
	//CenteredRotation(formattedImage, 1.92f, pixels);

	if (hough)
		Rotation(formattedImage, pixels);
	else
		CenteredRotation(formattedImage, rotation, pixels);

	memcpy(pixels2, pixels, formattedImage->h*formattedImage->w*
	       sizeof(unsigned int));

	RLSA(formattedImage, pixels2, 22);

	//int tmpH = 0;
	//int highestH = 0;

	RECTANGLE* lines = calloc(formattedImage->h*formattedImage->w,sizeof(RECTANGLE));
	RECTANGLE* characters = calloc(formattedImage->h*formattedImage->w
	                               ,sizeof(RECTANGLE));

	//Lines Detection
	int linesNb = 0;
	DetectBlockFromACertainXY(formattedImage, pixels2, lines, 0, 0, &linesNb);

	int linesHeightMean = 0;
	//int linesWidthMean = 0;

	for (int i = 0; i < linesNb; i++)
	{
		linesHeightMean = (linesHeightMean + lines[i].BR.Y - lines[i].TL.Y)/2;
		//linesWidthMean = (linesWidthMean + lines[i].BR.X - lines[i].TL.X)/2;
	}
	//  free(lines);

	/*while (++tmpH <= formattedImage->h)
	{
		if (linesNb >= formattedImage->h)
		{
			printf("OpenCR: Fatal Error!\n");
			exit(EXIT_FAILURE);
		}
		lines[linesNb] = DetectLineFromACertainXY(image, pixels2, 1, tmpH);
		tmpH = lines[linesNb].BR.Y;
		linesNb++;
		highestH = tmpH > highestH ? tmpH : highestH;

		if (tmpH < highestH)
			break;
	}*/

	//Character Detection

	printf("LinesNB = %d\n", linesNb);
	int tmpTLX = 0;
	int charsNb = 0;
	int index = 0;
	RECTANGLE tmpChar;
	for (int i = 0; i < linesNb; i++)
	{
		//if (lines[i].BR.Y - lines[i].TL.Y > linesHeightMean*7)
		// continue;

		tmpTLX = lines[i].TL.X;

		while (tmpTLX <= lines[i].BR.X-1 && index < formattedImage->w)
		{
			tmpChar =  DetectCharacterFromACertainLineAndX(
			               formattedImage, pixels, lines[i], tmpTLX);

			if (tmpChar.TL.X > 5 && tmpChar.BR.X > 5)
			{
				characters[charsNb] =  tmpChar;
				tmpTLX = characters[charsNb].BR.X+1;
				charsNb++;
			}
			index++;
		}
		index = 0;
	}

	printf("charsNb : %d\n", charsNb);

	/*for (int i = 0; i < linesNb; i++)
	{
		linesHeightMean = (linesHeightMean + characters[i].BR.Y - characters[i].TL.Y)/2;
		linesWidthMean = (linesWidthMean + characters[i].BR.X - characters[i].TL.X)/2;
	}

	for (int i = 0; i < linesNb; i++)
	{
		if (characters[i].BR.Y - characters[i].TL.Y < linesHeightMean/3 ||characters[i].BR.Y - characters[i].TL.Y > linesHeightMean*7)
		{
			characters[i].BR.X = 0;
			characters[i].BR.Y = 0;
			characters[i].TL.X = 0;
			characters[i].TL.Y = 0;
		}

	}*/

	//Neural Network
#if !DEBUG

	/*	char patterns_2[PATTERN_Y][PATTERN_X] =
		{
			{' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
			{' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
			{' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
			{' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
			{' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
			{' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
			{' ', ' ', ' ', ' ', ' ', ' ', ' ', ' '},
			{' ', ' ', ' ', ' ', 'O', 'O', 'O', ' '},
			{' ', ' ', 'O', 'O', ' ', ' ', 'O', 'O'},
			{' ', 'O', 'O', ' ', ' ', ' ', 'O', 'O'},
			{' ', 'O', 'O', ' ', ' ', 'O', 'O', ' '},
			{' ', 'O', ' ', 'O', '0', ' ', ' ', ' '},
			{'O', 'O', 'O', ' ', ' ', ' ', ' ', ' '},
			{'O', 'O', ' ', ' ', ' ', ' ', ' ', ' '},
			{' ', 'O', 'O', ' ', ' ', ' ', ' ', ' '},
			{' ', ' ', 'O', 'O', 'O', ' ', ' ', ' '},
		};
	*/
	//FILE* f = fopen("RESULT.txt", "w");
	//NETWORK network;

	//InitNetwork(&network);

	linesNb = 0;

	RECTANGLE* lines2 = calloc(charSetImage->h*formattedImage->w,sizeof(RECTANGLE));

	DetectBlockFromACertainXY(charSetImage, charSetImage->pixels, lines2, 0, 0, &linesNb);

	int patterns [CHARSET][PATTERN_Y][PATTERN_X]; //[CHARSET][PATTERN_Y][PATTERN_X]
	int patterns_ [charsNb*5][PATTERN_Y][PATTERN_X]; //[CHARSET][PATTERN_Y][PATTERN_X]

	printf("LinesNb : %d\n", linesNb);

	int charIndex=0;

	for (; charIndex < linesNb; charIndex++)
		for (int i = lines2[charIndex].TL.Y; i < lines2[charIndex].BR.Y; i++)
			for (int j = lines2[charIndex].TL.X; j < lines2[charIndex].BR.X; j++)
				patterns[charIndex][i-lines2[charIndex].TL.Y][j-lines2[charIndex].TL.X] = Get_Pixel(charSetImage, charSetImage->pixels, j, i) > BLACK ? 0 : 1;

	free(lines2);
	//for (int i = 0; i < PATTERN_Y; i++)
	//for (int j = 0; j < PATTERN_X; j++)
	//	patterns[2][i][j] = patterns_2[i][j];

	// unsigned int currentPattern_[PATTERN_XY];

	/*unsigned int intermediary[51*51];
	unsigned int resizedCurrentPattern[PATTERN_XY];
	int thumbwidth;
	int thumbheight;
	double xscale;
	double yscale;
	double threshold;
	double yend;*/
	for (charIndex = 0; charIndex < charsNb; charIndex++)
	{
		//printf("%d %d %d %d", characters[charIndex].BR.X, characters[charIndex].BR.Y, characters[charIndex].TL.X, characters[charIndex].TL.Y);

		unsigned int currentPattern [(characters[charIndex].BR.X - characters[charIndex].TL.X)*(characters[charIndex].BR.Y - characters[charIndex].TL.Y)];

		for (int i = characters[charIndex].TL.Y; i < characters[charIndex].BR.Y; i++)
		{
			//printf("\n");
			for (int j = characters[charIndex]. TL.X; j < characters[charIndex].BR.X; j++)
			{
				currentPattern[i*(characters[charIndex].BR.X - characters[charIndex].TL.X)+j] = Get_Pixel(formattedImage, pixels, j, i);
				//printf("%c", currentPattern[i*(characters[charIndex].BR.X - characters[charIndex].TL.X)+j] == BLACK ? 'O' : ' ');
			}
		}

		//printf("   charIndex : %d ,    charsNb : %d\n", charIndex, charsNb);
		//memcpy(resizedCurrentPattern, ResizePixels(currentPattern, (characters[charIndex].BR.X - characters[charIndex].TL.X), (characters[charIndex].BR.Y - characters[charIndex].TL.Y), PATTERN_X, PATTERN_Y), PATTERN_XY*sizeof(unsigned int));
		/*int w1 = characters[charIndex].BR.X - characters[charIndex].TL.X;
		int h1 = characters[charIndex].BR.Y - characters[charIndex].TL.Y;

		thumbwidth = 51;
		thumbheight = 51;
		xscale = (thumbwidth+0.0) / w1;
		 yscale = (thumbheight+0.0) / h1;
		 threshold = 0.5 / (xscale * yscale);
		 yend = 0.0;
		for (int f = 0; f < thumbheight; f++) // y on output
		{
			double ystart = yend;
			yend = (f + 1) / yscale;
			if (yend >= h1) yend = h1 - 0.000001;
			double xend = 0.0;
			for (int g = 0; g < thumbwidth; g++) // x on output
			{
				double xstart = xend;
				xend = (g + 1) / xscale;
				if (xend >= w1) xend = w1 - 0.000001;
				double sum = 0.0;
				for (int y = (int)ystart; y <= (int)yend; ++y)
				{
					double yportion = 1.0;
					if (y == (int)ystart) yportion -= ystart - y;
					if (y == (int)yend) yportion -= y+1 - yend;
					for (int x = (int)xstart; x <= (int)xend; ++x)
					{
						double xportion = 1.0;
						if (x == (int)xstart) xportion -= xstart - x;
						if (x == (int)xend) xportion -= x+1 - xend;
						sum += currentPattern[y*w1+x] * yportion * xportion;
					}
				}
				intermediary[f*thumbwidth+g] = (sum > threshold) ? 1 : 0;
			}
		}

			thumbwidth = PATTERN_X;
		thumbheight = PATTERN_Y;
		xscale = (thumbwidth+0.0) / 51;
		 yscale = (thumbheight+0.0) / 51;
		 threshold = 0.5 / (xscale * yscale);
		 yend = 0.0;
		for (int f = 0; f < thumbheight; f++) // y on output
		{
			double ystart = yend;
			yend = (f + 1) / yscale;
			if (yend >= h1) yend = h1 - 0.000001;
			double xend = 0.0;
			for (int g = 0; g < thumbwidth; g++) // x on output
			{
				double xstart = xend;
				xend = (g + 1) / xscale;
				if (xend >= w1) xend = w1 - 0.000001;
				double sum = 0.0;
				for (int y = (int)ystart; y <= (int)yend; ++y)
				{
					double yportion = 1.0;
					if (y == (int)ystart) yportion -= ystart - y;
					if (y == (int)yend) yportion -= y+1 - yend;
					for (int x = (int)xstart; x <= (int)xend; ++x)
					{
						double xportion = 1.0;
						if (x == (int)xstart) xportion -= xstart - x;
						if (x == (int)xend) xportion -= x+1 - xend;
						sum += intermediary[y*w1+x] * yportion * xportion;
					}
				}
				resizedCurrentPattern[f*thumbwidth+g] = (sum > threshold) ? 1 : 0;
			}
		}
*/
		/*int w1 = characters[charIndex].BR.X - characters[charIndex].TL.X;
		int h1 = characters[charIndex].BR.Y - characters[charIndex].TL.Y;

		for (int i = 0; i < PATTERN_Y-1; i++)
		    for (int j = 0; j < PATTERN_X-1; j++)
		        resizedCurrentPattern[i*PATTERN_X+j] = currentPattern[((i*h1)/PATTERN_Y)*w1+((j*w1)/PATTERN_X)];*/

		/*int x = PATTERN_X;
		int y = PATTERN_Y;
		//Find inverses of ratios
		float invxr = 1/((float)PATTERN_X/(float)w1);
		float invyr = 1/((float)PATTERN_Y/(float)h1);

		for (int j = 0; j < y; j++)
			for (int i = 0; i < x; i++)

				resizedCurrentPattern[(j*x)+i] = currentPattern[(int)(((j*invyr)*w1)+(i*invxr))];

		int w1 = characters[charIndex].BR.X - characters[charIndex].TL.X;
		 int x_ratio = (int)(((float)PATTERN_X)/(float)w1) +1;
		// int y_ratio = (int)(((float)PATTERN_Y)/(float)h1) +1;
		 //int x_ratio = (int)((w1<<16)/w2) ;
		 //int y_ratio = (int)((h1<<16)/h2) ;
		 int x2, y2 ;
		 for (int i=0;i<PATTERN_Y;i++) {
		     for (int j=0;j<PATTERN_X;j++) {
		         x2 = ((j*x_ratio));
		         y2 = ((i*y_ratio));
		         resizedCurrentPattern[(i*PATTERN_X)+j] = currentPattern[(y2*w1)+x2];
		     }
		 }

		unsigned int r = 0;

		for (int i = 0; i < PATTERN_Y; i++)
		    for (int j = 0; j < PATTERN_X; j++)
		    {
		        for (int a = 0; a < x_ratio; a++)
		            r+= currentPattern[i*PATTERN_X+j+a];
		        r/=x_ratio;
		        resizedCurrentPattern[i*PATTERN_X+j] = r;
		    }

		int w2 = PATTERN_X, h2 = PATTERN_Y;

		int A, B, C, D, x, y, gray ;
		float x_ratio = ((float)(w1))/w2 ;
		float y_ratio = ((float)(h1))/h2 ;
		float x_diff, y_diff;

		for (int i=0; i<h2; i++)
		{
			for (int j=0; j<w2; j++)
			{
				x = (int)(x_ratio * j) ;
				y = (int)(y_ratio * i) ;
				x_diff = (x_ratio * j) - x ;
				y_diff = (y_ratio * i) - y ;

				A = currentPattern[x +y*w1] & 0xff;

				B = currentPattern[ x+1+y*w1] & 0xff;

				C = currentPattern[ x + (y+1)*w1] & 0xff;

				D = currentPattern[x+1+ (y+1)*w1] & 0xff;

				gray = ((int)(A*(1-x_diff)*(1-y_diff) + B*(x_diff)*(1-y_diff) +
				              C*(y_diff)*(1-x_diff) + D*(x_diff*y_diff)));

				gray = gray < 127 ? 0 : 255;

				resizedCurrentPattern[j+i*w2] = gray;
			}
		}*/

		//		unsigned int pixels_written = 0;

		/*printf("Resized : \n");
		for (int i = 0; i < PATTERN_Y; i++)
		{
			printf("\n");
			for (int j = 0; j < PATTERN_X; j++)
				printf("%c", resizedCurrentPattern[i*PATTERN_X+j] == BLACK ? 'O' : ' ');
		}*/

		// Resize(formattedImage, currentPattern, &characters[charIndex], resizedCurrentPattern);
		//#pragma omp sections
		//	{
		//#pragma omp section
		//{
//#pragma omp_set_dynamic(8)
		//#pragma omp parallel for
		for (int i = 0; i < PATTERN_Y; i++)
			//#pragma omp parallel for private(patterns_)
			//#pragma omp parallel for shared(patterns_)
			for (int j = 0; j < PATTERN_X; j++)
				patterns_[charIndex][i][j] = currentPattern[(i+characters[charIndex].TL.Y)*(characters[charIndex].BR.X - characters[charIndex].TL.X)+(j+characters[charIndex].TL.X)] == BLACK ? 1 : 0;
           //     patterns_[charIndex][i][j] = resizedCurrentPattern[i*PATTERN_X+j];
		//}
		//}
	}

	ComputeNetwork(charsNb, patterns, patterns_, characters, outPutFileName);

	//for (int i = 0; i < CHARSET; i++)
	//{
	//srand(SEED);
	//InitApp(patterns, patterns_);
	//ComputeWeights(&network);

	//ComputeEverything(f, &network);
	//}
	//fclose(f);
	//ShutDownNetwork(&network);

//    free(lines2);
	free(lines);
	free(characters);

#endif // !DEBUG


	/*for (int i = 0; i < PATTERN_Y; i++)
		for (int j = 0; j < PATTERN_X; j++)
	        patterns_[0][i][j] = patterns[1][i][j];


	for (int i = 0; i < PATTERN_Y; i++)
		for (int j = 0; j < PATTERN_X; j++)
	        patterns_[1][i][j] = patterns_2[i][j] == 'O' ? 1 : 0;


	for (int i = 0; i < PATTERN_Y; i++)
		for (int j = 0; j < PATTERN_X; j++)
	        patterns_[2][i][j] = patterns_2[i][j] == 'O' ? 1 : 0;*/
//Only for previewing purpose
//unsigned int* tmpPixels = pixels;
	//pixels = pixels2;

	SDL_EventState(SDL_KEYDOWN, SDL_DISABLE);
	SDL_EventState(SDL_KEYUP, SDL_DISABLE);
	while(!quit)
		Update_OpenCR();

	free(pixels2);
//pixels = tmpPixels;

	exit(EXIT_SUCCESS);
}

void Exit_OpenCR(int err)
{
	if (err != 0)
		printf("Critical Error !\n");

	free(pixels);
	SDL_DestroyTexture(texture);
	SDL_FreeSurface(image);
	SDL_FreeSurface(formattedImage);
	SDL_FreeSurface(charSetImage);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	IMG_Quit();
	SDL_Quit();
}
