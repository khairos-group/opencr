#ifndef MEDIAN_FILTER_
#define MEDIAN_FILTER_

#include "util.h"
#include "graphics.h"

void MedianFilter(SDL_Surface* image, unsigned int* pixels, int N);
void tri_select(unsigned int* a, int array_size);

#endif
