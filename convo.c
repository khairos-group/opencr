#include "convo.h"
#include <string.h>

void convo(SDL_Surface* image, unsigned int* pixels, int size, int *mat_convo)
{
	int h = image->h;
	int w = image->w;

	unsigned int* pixels2 = calloc(w*h,sizeof(unsigned int));

	Uint8 r,g,b;

for (int i = (size/2)+1; i <= w-(size/2)-1; i++)
		for (int j = (size/2)+1; j <= h-(size/2)-1; j++)
		{
           
            int res = 0;
            int x = 0;  
			for (int k = i-(size/2); k <= i+(size/2); k++)
			{
                #pragma omp for
				for (int l = j-(size/2); l <= j+(size/2); l++)
				{
			        Uint32 pp = Get_Pixel(image, pixels, l, k);
					SDL_GetRGB(pp, image->format, &r,&g,&b);
                  //  printf("r = %d\n",r);
                    res = mat_convo[x] * r + res;
                  //   printf("val = %d\n", res);
                  x++;
       			
                }
            }
                int div =0;
                int y =0;
                for(; y < size*size; y++)
                    {div = div + mat_convo[y];} 
                if(div != 0)
                    {res =res/div;}
               
       //  printf("val final = %d\n", res);
          
            PIXEL ppp = {{res,res,res,255}, j, i};
			Set_Pixel(image, ppp, pixels2);
   }

	memcpy(pixels, pixels2, w*h*sizeof(unsigned int));
	free(pixels2);
}


