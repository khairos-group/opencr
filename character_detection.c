#include "character_detection.h"

void DetectBlockFromACertainXY(SDL_Surface* image, unsigned int* pixels,
                               RECTANGLE* lines, int X, int Y, int* linesNB)
{
	X = X+0;
	Y = Y+0;

	int w = image->w;
	int h = image->h;

	int components = 0;

	long* labels = calloc(w*h,sizeof(long));

	unsigned int* pixels2 = calloc(w*h,sizeof(unsigned int));

	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
			pixels2[i*w+j] = Get_Pixel(image, pixels, j, i) == BLACK ? 0 : 255;

	Label(labels, pixels2, w, h);

	free(pixels2);


	for (int i = 1; i < h; i++)
		for (int j = X; j < w; j++)
			components = labels[j+i*w] > components ? labels[j+i*w] : components;

	int index = 0;
	int maxX, maxY, minX, minY;
	maxX = maxY = 0;
	minX = w;
	minY = h;

	int currentPos = 0;

	for (int a = 1; a < components+1; a++)
	{
		for (int i = 1; i < h; i++)
			for (int j = 0; j < w; j++)
			{
				currentPos = j+i*w;

				if (labels[currentPos] == a)
				{
					maxX = j > maxX ? j : maxX;
					maxY = i > maxY ? i : maxY;
					minX = j < minX ? j : minX;
					minY = i < minY ? i : minY;
				}
				//currentLabel = labels[i].label;
			}
		//printf("%d %d %d %d\n", maxX, maxY, minX, minY);

		lines[index].BR.X = maxX;
		lines[index].BR.Y = maxY;
		lines[index].TL.X = minX;
		lines[index].TL.Y = minY;

		index++;
		maxX = maxY = 0;
		minX = w;
		minY = h;
	}

	*linesNB = components;
	//printf("Components : %d\n", components);

	// memcpy(lines, rectangles, components*sizeof(RECTANGLE));

	// for (int i = 0; i < components; i++)
	// lines[i] = rectangles[i];

	free(labels);
}

RECTANGLE DetectLineFromACertainXY(SDL_Surface* image, unsigned int* pixels ,
                                   int X, int Y)
{
	int h = image->h;
	int w = image->w;

	POINT lowestWidth = {w, 0};
	int highestWidth = 0;
	int bottomLineHeight = 0;

	Uint32 currentPixel;
	POINT highestHeightOnCurrentLine;
	highestHeightOnCurrentLine.X = 0;
	highestHeightOnCurrentLine.Y = 0;
	int blackPixelsCount = 0;
	int whitePixelsCount = 0;
	RECTANGLE line;

	for (int i = Y; i < h; i++)
		for (int j = X; j < w; j++)
		{
			currentPixel = Get_Pixel(image, pixels, j, i);
			if (currentPixel == BLACK)
			{
				whitePixelsCount = 0;
				lowestWidth.X = lowestWidth.X != 0 && j < lowestWidth.X ? j :
				                lowestWidth.X;
				//lowestWidth.Y = i;
				if (blackPixelsCount == 0)
				{
					blackPixelsCount++;
					highestHeightOnCurrentLine.X = j;
					highestHeightOnCurrentLine.Y = i;
				}
			}
			else
			{
				if (j == 0)
					whitePixelsCount = 0;
				if (blackPixelsCount == 1)
				{
					whitePixelsCount++;

					if (whitePixelsCount >= w)
					{
						bottomLineHeight = i-1;
						goto end;
					}
				}
			}
		}
end:

	line.TL.X = lowestWidth.X+1;
	line.TL.Y = highestHeightOnCurrentLine.Y;

	line.BR.Y = bottomLineHeight;

	whitePixelsCount = 0;

	/*int i = 0;
	int flag = 0;
	for (i = line.TL.Y+2; i < h; i++)
		for (int j = line.TL.X; j < w; j++)
		{
			if (j == 0 && flag == 0 && whitePixelsCount == w - line.TL.X)
			{
				line.BR.Y = i-1;
				flag = 1;
				break;
			}
			if (j==0)
				whitePixelsCount = 0;
	    else if (Get_Pixel(image, pixels, j, i) == WHITE)whitePixelsCount++;
		}*/



	for (int i = line.TL.Y; i < line.BR.Y; i++)
		#pragma omp for
		for (int j = w; j > line.TL.X; j--)
			if (Get_Pixel(image, pixels, j, i) == BLACK)
				highestWidth = j >= highestWidth ? j : highestWidth;

	line.BR.X = highestWidth+1;

//#if DEBUG
	//printf("\n Rectangle : %d %d %d %d\n", line.TL.X, line.TL.Y, line.BR.X,
	// line.BR.Y);

	PIXEL red = {{255, 0, 0, 255}, 0, 0};

	for (int i = line.TL.Y; i < line.BR.Y; i++)
		for (int j = line.TL.X; j < line.BR.X; j++)
		{
			if (i <= line.TL.Y + 1 || j <= line.TL.X + 1|| i >= line.BR.Y-2||
			        j >= line.BR.X-2)
			{
				red.X = j;
				red.Y = i;
				Set_Pixel(image, red, pixels);
			}
		}
//#endif // DEBUG

	return line;
}

/*RECTANGLE DetectLineFromACertainXY_2(SDL_Surface* image, unsigned int* pixels ,
                                   int X, int Y)
{
    int h = image->h;
	int w = image->w;

	RECTANGLE line;

	for (int i = Y; i < h; i++)
		for (int j = X; j < w; j++)
            ;

}*/

RECTANGLE DetectCharacterFromACertainLineAndX(SDL_Surface* image,
        unsigned int* pixels,
        RECTANGLE line, int X)
{
	//int startW = line.TL.X;
	int endW = line.BR.X;
	int startH = line.BR.Y;
	int endH = line.TL.Y;

	//RECTANGLE characters[(endW-startW)/3];

	int whitePixelsCount = 0;

	RECTANGLE character;

	Uint32 currentPixel;

	int firstWidth = 0;
	int lastWidht = 0;
	int blackPixelsCount = 0;

	int i = 0;
	int j = 0;

	//character.TL.Y = endH;

	for (i = X; i < endW; i++)
		for (j = startH; j > endH; j--)
		{
			currentPixel = Get_Pixel(image, pixels, i, j);

			if (currentPixel != BLACK)
				whitePixelsCount++;

			if (currentPixel == BLACK && blackPixelsCount == 0)
			{
				blackPixelsCount++;
				firstWidth = i;
				//character.TL.Y = j < character.TL.Y ? j : character.TL.Y;
			}

			lastWidht = i > lastWidht ? i : lastWidht;

			if (j == startH)
			{
				if (blackPixelsCount && whitePixelsCount >= startH - endH)
					goto end2;

				whitePixelsCount = 0;
			}
		}

end2:

	character.BR.X = lastWidht;
	character.BR.Y = startH+1;

	character.TL.X = firstWidth;
	character.TL.Y = endH;
//
	/*character.BR.Y = 0;

	blackPixelsCount = 0;
	whitePixelsCount = 0;

	for (i = firstWidth; i < lastWidht; i++)
	    for (j = endH; character.TL.Y; j++)
	    {
	        currentPixel = Get_Pixel(image, pixels, i, j);

			if (currentPixel != BLACK)
				whitePixelsCount++;

			if (currentPixel == BLACK && blackPixelsCount == 0)
			{
				blackPixelsCount++;
				firstWidth = i;
				character.BR.Y = j > character.BR.Y ? j : character.BR.Y;
			}

			lastWidht = i > lastWidht ? i : lastWidht;

			if (j == startH)
			{
				if (blackPixelsCount && whitePixelsCount >= startH - endH)
					goto end3;

				whitePixelsCount = 0;
			}
	    }
	    end3:*/

//#if DEBUG

//	printf("\n Character rectangle : %d %d %d %d\n", character.TL.X,
//	       character.TL.Y, character.BR.X,
	//   character.BR.Y);





//#endif // DEBUG

	if (character.TL.X != 0)
	{
		PIXEL blue = {{0, 0, 255, 255}, 0, 0};

		for (int i = character.TL.Y; i < character.BR.Y; i++)
			for (int j = character.TL.X; j < character.BR.X; j++)
			{
				if (i <= character.TL.Y || j <= character.TL.X||
				        i >= character.BR.Y-1||
				        j >= character.BR.X-1)
				{
					blue.X = j;
					blue.Y = i;
					Set_Pixel(image, blue, pixels);
				}
			}
		return character;
	}
	else
	{
		RECTANGLE r = {{0, 0}, {0, 0}};
		return r;
	}
}

void RLSA(SDL_Surface* image, unsigned int* pixels, int threshold)
{
	int w = image->w;
	int h = image->h;
	unsigned int* pixels2 = malloc(w*h*sizeof(unsigned int));

	int hor_thres = threshold;
	int zero_count = 0;
	int one_flag = 0;

	Uint8 r, g, b;

	Uint32 currentPixel = 0;

	for (int i = 0; i<h; i++)
	{
		for (int j = 0; j<w; j++)
		{
			currentPixel = Get_Pixel(image, pixels, j, i);
			SDL_GetRGB(currentPixel, image->format, &r, &g, &b);
			if (r == 0)
			{
				if (one_flag == 255)
				{
					if (zero_count <= hor_thres)
					{
						for (int k = i; k <= i+1; k++)
							for (int l = j - zero_count; l <= j; l++)
							{

								PIXEL p = {{0, 0, 0, 255}, l, k};
								Set_Pixel(image, p, pixels2);
							}
					}
					else
					{
						one_flag = 0;
					}
					zero_count = 0;
				}
				one_flag = 255;
			}
			else
			{
				if (one_flag == 255)
					zero_count++;
			}
		}
	}

	memcpy(pixels, pixels2, w*h*sizeof(unsigned int));
	free(pixels2);
}
