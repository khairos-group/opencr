#include "detect_rotation.h"
#include <math.h>

float DegToRadians(float deg)
{
	return (float)(deg / 180 * M_PI);
}

float RadToDegrees(float rad)
{
	return (float)(rad / M_PI * 180);
}

// Fill the histogram
/*void HoughLineDetection(SDL_Surface* image, unsigned int* pixels, int *histogram)
{
	int h = image->h;
	int w = image->w;
	int thetaDeg;
	int thetaRad;
	float rho;

	for(int i = 0; i < w; i++)
	{
		for(int j = 0; j < h; j++)
		{
			if(Get_Pixel(image, pixels, i, j) == BLACK)
			{
				for(thetaDeg = 0; thetaDeg < 180; thetaDeg++)
				{
					thetaRad = DegToRadians(thetaDeg);
					rho = i*cosf(thetaRad)+j*sinf(thetaRad);
					histogram[(int)(rho + (thetaDeg * (w + h)))]++;
				}
			}
		}
	}
}*/

int HoughLineDetection(SDL_Surface* image, unsigned int* pixels)
{
	int h = image->h;
	int w = image->w;

	//Uint8 r,g,b;
	int thetaFix = 360;

	int *accu[thetaFix];
	for (int i=0; i<thetaFix; i++)
		accu[i]=calloc(h*w,sizeof(int));

	float rho;
	float thetaRadian;
	int thetaMax = 0;

	for (int y=0; y<h; y++)
		for (int x=0; x<w; x++)
		{
			//SDL_GetRGB(pixel, image->format, &r, &g, &b);
			if (Get_Pixel(image, pixels, x, y)==BLACK)
				for (int theta=0; theta<thetaFix; theta++)
				{
					thetaRadian = DegToRadians(theta);
					rho=x*cosf(thetaRadian)+y*sinf(thetaRadian);
					accu[theta][(int)fabs(rho)]++;
				}
		}

	int max = 0;
	for (int theta=0; theta<thetaFix; theta++)
		for (int i=0; i<h*w; i++)
			if (accu[theta][i]>max)
			{
				thetaMax = theta;
				max = accu[theta][i];
			}

	for (int i=0; i<thetaFix; i++)
		free(accu[i]);

	return thetaMax;
}


// Rotation
void CenteredRotation(SDL_Surface* image, float angle, unsigned int* pixels)
{
	int x, y;
	int cx = image->w/2;
	int cy = image->h/2;
	float angleRad = DegToRadians(angle);

	unsigned int* newPixels = malloc(image->w*image->h*sizeof(unsigned int));
	memset(newPixels, -1, image->w*image->h*sizeof(unsigned int));


	for(int j = 0; j < image->h; j++)
		for(int i = 0; i < image->w; i++)
		{
			x = (int) (cos(angleRad) * (i - cx) + sin(angleRad) * (j - cy)) + cx;
			y = (int) (-sin(angleRad) * (i - cx) + cos(angleRad) * (j - cy)) + cy;

			if (Get_Pixel(image, pixels, x, y) == BLACK && x >= 0 && x < image->w && y >= 0 && y < image->h)
			{
				PIXEL p = {{0,0,0, 255},i,j};
				Set_Pixel(image, p, newPixels);
			}
		}

	memcpy(pixels, newPixels, image->w*image->h*sizeof(unsigned int));
	free(newPixels);
}

void Rotation(SDL_Surface* image, unsigned int* pixels)
{
	CenteredRotation(image, HoughLineDetection(image, pixels), pixels);
}
