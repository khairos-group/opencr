#ifndef OPEN_CR_
#define OPEN_CR_

#include "util.h"

void Init_OpenCR(int argc, char** argv);
void Run_OpenCR(float rotation, const char* outPutFileName, bool hough, bool filters);
void Exit_OpenCR();

#endif
