#include "median_filter.h"
#include <string.h>

void MedianFilter(SDL_Surface* image, unsigned int* pixels, int N)
{
	int h = image->h;
	int w = image->w;

	unsigned int* pixels2 = calloc(w*h,sizeof(unsigned int));

	Uint8 r,g,b;
	unsigned int* list = calloc(N*N, sizeof(unsigned int));

	for (int i = 1; i < w-1; i++)
		for (int j = 1; j <h-1; j++)
		{
            int m = 0; 
			for (int k = i-1; k <= i+1; k++)
				#pragma omp for
				for (int l = j-1; l <= j+1; l++)
				{
					Uint32 pp = Get_Pixel(image, pixels, l, k);
					SDL_GetRGB(pp, image->format, &r,&g,&b);
					list[m] = r;
               //   printf("valeur %d du tableau = %d\n", m, r);
                    m = m+1;
                    

				}

			tri_select(list, N*N);
			int mil = list[(N*N)/2];
        //   printf("milieu = %d\n", mil); // test de print du milieu de list 
			PIXEL ppp = {{mil,mil,mil,255}, j, i};
			Set_Pixel(image, ppp, pixels2);
   		}

	memcpy(pixels, pixels2, w*h*sizeof(unsigned int));
	free(list);
	free(pixels2);
}

void tri_select(unsigned int* a, int array_size)
    {
        int i, min, j, tmp;
        for(i = 0 ; i < array_size-1; i++)
        {
            min = i;
            for(j = i+1; j < array_size; j++)
               { 
                    if(a[j] < a[min])
                        { min = j;}
               }
            if(min  != i)
            {
                tmp = a[i];
                a[i] = a[min];
                a[min] = tmp;
                }
                    
            }
        }

