#ifndef UNION_FIND_
#define UNION_FIND_

#include "util.h"

#define NO_ROOT -1

typedef struct UNION_FIND
{
    int* labels;
    int capacity;
    int size;

    int label;
}UNION_FIND;


long Find(long* labels, long pos);
long Union(long* labels, long root0, long root1);
long Add(long* labels, long pos, long root);
void Label(long* labels,unsigned int* image, int W, int H);


#endif
