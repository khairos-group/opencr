#include "graphic_interface.h"

static void print()
{
	g_print ("Clicked on 'Process...'\n");
}

static gboolean on_delete_event ()
{
	g_print ("Closed Window\n");
	exit(0);

	return TRUE;
}

const char* outputName = NULL;
const char* angleChar = NULL;
const char* charSetPath = NULL;
const char* imagePath = NULL;
bool houghToggled = false;
bool noiseToggled = false;

static void on_textboxOutput(GtkEntry *entry)
{
	outputName = gtk_entry_get_text(GTK_ENTRY(entry));
}

static void on_textboxAngle(GtkEntry *entry)
{
	angleChar = gtk_entry_get_text(GTK_ENTRY(entry));
}

static void fonctionhough()
{
    houghToggled = houghToggled == false ? true : false;
}

static void fonctionnoise()
{
    noiseToggled = noiseToggled == false ? true : false;
}

static void func_charSetPath(GtkFileChooser *chooser)
{
    charSetPath = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser));
}

static void func_imagePath(GtkFileChooser *chooser)
{
    imagePath = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(chooser));
}

void Init_GTK(int argc, char** argv, const char** outPutFileName, const char** angle, const char** charsetPath, const char** imagepath, bool* hough, bool* filters)
{
	GtkWidget *window;

	GtkWidget *logo;

	GtkWidget *charsetLabel;
	GtkWidget *charsetChooser;
	GtkWidget *imageLabel;
	GtkWidget *imageChooser;
	GtkWidget *processButton;

	GtkWidget *houghLabel;
	GtkWidget *houghCheck;

	GtkWidget *noiseLabel;
	GtkWidget *noiseCheck;

	GtkWidget *box1;
	GtkWidget *box2;
	GtkWidget *box3;
	GtkWidget *box4;
	GtkWidget *box5;
	GtkWidget *box6;
	GtkWidget *box7;
	GtkWidget *box8;
	GtkWidget *box9;
	GtkWidget *box10;
	GtkWidget *box11;

	GtkWidget *unitLabel;

	GtkWidget *outputLabel;
	GtkWidget *outputEntry;

	GtkWidget *angleLabel;
	GtkWidget *angleEntry;

	GtkWidget *vSeparator;

	//////////////////////
	gtk_init (&argc, &argv);

	//////////////////////
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW (window), "OpenCR");

	logo = gtk_image_new_from_file("logo.png");
	gtk_widget_set_halign(GTK_WIDGET(logo), GTK_ALIGN_START);
	gtk_widget_set_margin_bottom(GTK_WIDGET(logo), 40);

	outputLabel = gtk_label_new("Output :");
	gtk_widget_set_halign(GTK_WIDGET(outputLabel), GTK_ALIGN_START);
	outputEntry = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(outputEntry), "RESULT.txt");

	angleLabel = gtk_label_new("Angle :");
	gtk_widget_set_halign(GTK_WIDGET(angleLabel), GTK_ALIGN_START);
	angleEntry = gtk_entry_new();
	gtk_entry_set_text(GTK_ENTRY(angleEntry), "0");

	noiseLabel = gtk_label_new("Filters");
	gtk_widget_set_halign(GTK_WIDGET(noiseLabel), GTK_ALIGN_START);
	houghLabel = gtk_label_new("Hough");
	unitLabel = gtk_label_new("°");

	/////////////////////
	gtk_container_set_border_width (GTK_CONTAINER (window), 100);

	box1 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 30);
	box2 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 10);
	box3 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 10);
	gtk_widget_set_margin_top(GTK_WIDGET(box3), 10);
	box4 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 10);
	box5 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 5);
	box6 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 5);
	box7 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
	box8 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 10);
	box9 = gtk_box_new (GTK_ORIENTATION_VERTICAL,10);
	box10 = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 10);
	box11 = gtk_box_new (GTK_ORIENTATION_VERTICAL, 10);

	houghCheck = gtk_check_button_new();
	noiseCheck = gtk_check_button_new();

	imageLabel = gtk_label_new("Pick an image");
	imageChooser = gtk_file_chooser_button_new("Pick an Image",
	               GTK_FILE_CHOOSER_ACTION_OPEN);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(imageChooser),
	                                    "~");
	charsetLabel = gtk_label_new("Choose a CharSet");
	charsetChooser = gtk_file_chooser_button_new("Select a CharSet",
	                 GTK_FILE_CHOOSER_ACTION_OPEN);
	gtk_file_chooser_set_current_folder(GTK_FILE_CHOOSER(charsetChooser),
	                                    "~");
	processButton = gtk_button_new_with_label ("Process...");

	vSeparator = gtk_separator_new(GTK_ORIENTATION_VERTICAL);

	///////////////////////
	g_signal_connect (window, "delete-event", G_CALLBACK (on_delete_event), NULL);

	g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);


	g_signal_connect (processButton, "clicked", G_CALLBACK (print), NULL);

	g_signal_connect_swapped (processButton, "clicked", G_CALLBACK (gtk_widget_destroy), window);
//	const char* charSetPath;
	g_signal_connect (outputEntry, "activate", G_CALLBACK (on_textboxOutput), NULL);
	g_signal_connect (angleEntry, "activate", G_CALLBACK (on_textboxAngle), NULL);

	g_signal_connect(GTK_TOGGLE_BUTTON(houghCheck), "toggled", G_CALLBACK(fonctionhough), NULL);
    g_signal_connect(GTK_TOGGLE_BUTTON(noiseCheck), "toggled", G_CALLBACK(fonctionnoise), NULL);

    g_signal_connect (GTK_FILE_CHOOSER(charsetChooser),
	                    "file-set",
	                    G_CALLBACK(func_charSetPath),
	                     NULL);
    g_signal_connect (GTK_FILE_CHOOSER(imageChooser),
	                    "file-set",
	                    G_CALLBACK(func_imagePath),
	                     NULL);

	////////////////////////
	gtk_box_pack_start (GTK_BOX(box4), outputLabel, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box4), outputEntry, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box6), houghLabel, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box6), houghCheck, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box7), angleEntry, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box7), unitLabel, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box5), angleLabel, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box5), box7, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box8), box5, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box8), box6, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box9), noiseLabel, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box9), noiseCheck, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box2), box4, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box2), box8, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box2), box9, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box10), vSeparator, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box3), imageLabel, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box3), imageChooser, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box3), charsetLabel, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box3), charsetChooser, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box3), processButton, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box1), box2, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box1), box10, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box1), box3, FALSE, FALSE, 0);

	gtk_box_pack_start (GTK_BOX(box11), logo, FALSE, FALSE, 0);
	gtk_box_pack_start (GTK_BOX(box11), box1, FALSE, FALSE, 0);

	gtk_container_add (GTK_CONTAINER (window), box11);

	///////////////////////
	gtk_widget_show (box1);
	gtk_widget_show (box2);
	gtk_widget_show (box3);
	gtk_widget_show (box4);
	gtk_widget_show (box5);
	gtk_widget_show (box6);
	gtk_widget_show (box7);
	gtk_widget_show (box8);
	gtk_widget_show (box9);
	gtk_widget_show (box10);
	gtk_widget_show (box11);

	gtk_widget_show (noiseLabel);
	gtk_widget_show (noiseCheck);

	gtk_widget_show (houghLabel);
	gtk_widget_show (houghCheck);

	gtk_widget_show (unitLabel);
	gtk_widget_show (outputLabel);
	gtk_widget_show (outputEntry);
	gtk_widget_show (angleLabel);
	gtk_widget_show (angleEntry);

	gtk_widget_show (imageLabel);
	gtk_widget_show (imageChooser);
	gtk_widget_show (charsetLabel);
	gtk_widget_show (charsetChooser);
	gtk_widget_show (processButton);

	gtk_widget_show (vSeparator);

	gtk_widget_show (logo);

	gtk_widget_show (window);


	gtk_main();

	///////////:

    *outPutFileName = outputName;
    *angle = angleChar;
    *charsetPath = charSetPath;
    *imagepath = imagePath;
    *hough = houghToggled;
    *filters = noiseToggled;
}
