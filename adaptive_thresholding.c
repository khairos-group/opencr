#include "adaptive_thresholding.h"
#include "graphics.h"

void Grayscale(SDL_Surface* image, unsigned int* pixels)
{
	int h = image->h;
	int w = image->w;

	Uint8 r,g,b;

	#pragma omp for
	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
		{
			Uint32 pp = Get_Pixel(image, pixels, j, i);
			SDL_GetRGB(pp, image->format, &r,&g,&b);
			Uint8 v = (r+g+b)/3;

			PIXEL p = {{v, v, v, 255}, j, i};
			Set_Pixel(image, p ,pixels);
		}
}

void SimpleBinarize(SDL_Surface* image, unsigned int* pixels)
{
	int h = image->h;
	int w = image->w;

	Uint8 r,g,b;

	#pragma omp for
	for (int i = 0; i < h; i++)
		for (int j = 0; j < w; j++)
		{
			Uint32 pp = Get_Pixel(image, pixels, j, i);
			SDL_GetRGB(pp, image->format, &r,&g,&b);


			if ((0.299*r+0.587*g+0.114*b) > 128)
			{
				PIXEL p  = {{255, 255, 255, 255}, j, i};
				Set_Pixel(image,p ,pixels);
			}
			else
			{
				PIXEL p = {{0, 0, 0, 255}, j, i};
				Set_Pixel(image, p ,pixels);
			}
		}
}

void Otsu(SDL_Surface* image, unsigned int* pixels)
{
	int h = image->h;
	int w = image->w;
	unsigned int* pixels2 = malloc(w*h*sizeof(unsigned int));
	int hist[GRAYLEVEL];
	double prob[GRAYLEVEL], omega[GRAYLEVEL];
	double myu[GRAYLEVEL];
	double max_sigma, sigma[GRAYLEVEL];
	int i, x, y;
	Uint8 r,g,b;
	int threshold;

	for (i = 0; i < GRAYLEVEL; i++)
		hist[i] = 0;

	for (y = 0; y < h; y++)
		for (x = 0; x < w; x++)
		{
			Uint32 pp = Get_Pixel(image, pixels, x, y);
			SDL_GetRGB(pp, image->format, &r,&g,&b);
			hist[(int)(0.299*r+0.587*g+0.114*b)]++;
		}

	for ( i = 0; i < GRAYLEVEL; i ++ )
		prob[i] = (double)hist[i] / (w * h);

	omega[0] = prob[0];
	myu[0] = 0.0;
	for (i = 1; i < GRAYLEVEL; i++)
	{
		omega[i] = omega[i-1] + prob[i];
		myu[i] = myu[i-1] + i*prob[i];
	}


	threshold = 0;
	max_sigma = 0.0;
	for (i = 0; i < GRAYLEVEL-1; i++)
	{
		if (omega[i] != 0.0 && omega[i] != 1.0)
			sigma[i] = pow(myu[GRAYLEVEL-1]*omega[i] - myu[i], 2) /
			           (omega[i]*(1.0 - omega[i]));
		else
			sigma[i] = 0.0;
		if (sigma[i] > max_sigma)
		{
			max_sigma = sigma[i];
			threshold = i;
		}
	}

	threshold -= 15;
	for (y = 0; y < h; y++)
		for (x = 0; x < w; x++)
		{
			Uint32 pp = Get_Pixel(image, pixels, x, y);
			SDL_GetRGB(pp, image->format, &r,&g,&b);
			if (r > threshold)
			{
				PIXEL p  = {{255, 255, 255, 255}, x, y};
				Set_Pixel(image,p ,pixels2);
			}
			else
			{
				PIXEL p  = {{0, 0, 0, 255}, x, y};
				Set_Pixel(image,p ,pixels2);
			}
		}
	memcpy(pixels, pixels2, w*h*sizeof(unsigned int));
	free(pixels2);
}
