#ifndef ADAPTIVE_THRESHOLDING_
#define ADAPTIVE_THRESHOLDING_

#include "util.h"

#define MAX_BRIGHTNESS  255
#define GRAYLEVEL       256

void Otsu(SDL_Surface* image, unsigned int* pixels);
void Grayscale(SDL_Surface* image, unsigned int* pixels);
void SimpleBinarize(SDL_Surface* image, unsigned int* pixels);
int Histogram(int *hist, SDL_Surface* image, unsigned int* pixels);
int OtsuLevel(int *hist, int n);

#endif
