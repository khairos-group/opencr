#include "graphics.h"

void Set_Pixel(SDL_Surface* image, PIXEL pixel, unsigned int* pixels)
{
	//pixels[pixel.Y * 1600 + pixel.X] = (0xFF << 24) | (pixel.color.r << 16) |
	// (pixel.color.g << 8) | (pixel.color.b);

	int bpp = image->format->BytesPerPixel;

	Uint8 *p = (Uint8 *)pixels + pixel.Y * image->pitch + pixel.X * bpp;

	Uint32 pixell = SDL_MapRGB(image->format, pixel.color.r, pixel.color.g,
	                           pixel.color.b);

	switch(bpp)
	{
	case 1:
		*p = pixell;
		break;

	case 2:
		*(Uint16 *)p = pixell;
		break;

	case 3:
		if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
		{
			p[0] = (pixell >> 16) & 0xff;
			p[1] = (pixell >> 8) & 0xff;
			p[2] = pixell & 0xff;
			//PIXEL p = {{255,pixels[y*w+x] >> 8, pixels[y*w+x] >> 16,
			//pixels[y*w+x]>>24},x,y};
		}
		else
		{
			p[0] = pixell & 0xff;
			p[1] = (pixell >> 8) & 0xff;
			p[2] = (pixell >> 16) & 0xff;
		}
		break;

	case 4:
		*(Uint32 *)p = pixell;
		break;
	}
}

Uint32 Get_Pixel(SDL_Surface* image, unsigned int* pixels, int x, int y)
{
	int bpp = image->format->BytesPerPixel;

	Uint8 *p = (Uint8 *)pixels + y * image->pitch + x * bpp;

	switch(bpp)
	{
	case 1:
		return *p;

	case 2:
		return *(Uint16 *)p;

	case 3:
		if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
			return p[0] << 16 | p[1] << 8 | p[2];
		else
			return p[0] | p[1] << 8 | p[2] << 16;

	case 4:
		return *(Uint32 *)p;

	default:
		return 0;
	}

}

unsigned int* ResizePixels(unsigned int pixels[], int w1, int h1, int w2, int h2)
{
    unsigned int* dest = malloc(w2*h2*sizeof(unsigned int));

	if (w2 == 0 ||h2 == 0)
		memset(dest, -1, w2*h2);
	else
	{
	printf(" Resized : \n");
	for (int i = 0; i < h2; i++)
		{
			printf("\n");
			for (int j = 0; j < w2; j++)
			{
				printf("%c", pixels[i*w2+j] == BLACK ? 'O' : ' ');
			}
		}

		int x_ratio = (int)((w1)/w2) +1;
		int y_ratio = (int)((h1)/h2) +1;

		int x2, y2 ;
		for (int i=0; i<h2; i++)
			for (int j=0; j<w2; j++)
			{
				x2 = (j*x_ratio);
				y2 = (i*y_ratio);
				dest[(i*w2)+j] = pixels[(y2*w1)+x2];
			}
	}

    return dest;
}

unsigned char GetPixelComp32(unsigned int* pixels, int x, int y, int c)
{
	unsigned char *p = ((unsigned char*)pixels) + y + x * 4;
	return p[c];
}

void PutPixelComp32(unsigned int* pixels, int x, int y, int c, unsigned char val)
{
	unsigned char *p = ((unsigned char*)pixels) + y + x * 4;
	p[c] = val;
}

void Stretch_Nearest(unsigned int* src, unsigned int* dest, int w1, int h1, int w2, int h2)
{
	int i,j,k;
	double rx,ry;
	unsigned char pix;
	rx = w2*1.0/w1;
	ry = h2*1.0/h1;
	for(i=0; i<w2; i++)
		for(j=0; j<h2; j++)
			for(k=0; k<3; k++)
			{
				pix = GetPixelComp32(src,(int)(i/rx),(int)(j/ry),k);
				PutPixelComp32(dest,i,j,k,pix);
			}
}

void DrawPixels(unsigned int* pixels, int pitch, SDL_Texture* pixelsTexture,
                SDL_Renderer* renderer)
{
	SDL_RenderClear(renderer);

	SDL_UpdateTexture(pixelsTexture, NULL, pixels, pitch);

	SDL_RenderCopy(renderer, pixelsTexture, NULL, NULL);

	SDL_RenderPresent(renderer);
}
