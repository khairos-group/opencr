#include "main.h"
#include "open_cr.h"
#include <stdio.h>
//#include <sys/resource.h>

int main(int argc, char** argv)
{

	/*const rlim_t kStackSize = 64 * 1024 * 1024;
	struct rlimit rl;
	int result;

	result = getrlimit(RLIMIT_STACK, &rl);
	if (result == 0)
	{
		if (rl.rlim_cur < kStackSize)
		{
			rl.rlim_cur = kStackSize;
			result = setrlimit(RLIMIT_STACK, &rl);
			if (result != 0)
			{
				fprintf(stderr, "setrlimit returned result = %d\n", result);
			}
		}
	}*/

	SET_OpenCR_THREADS_NUM(8);

	Init_OpenCR(argc, argv);

	return 0;
}

void Exit(void)
{
	Exit_OpenCR(EXIT_SUCCESS);
	printf("Exiting OpenCR...\n");
}
