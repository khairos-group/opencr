#ifndef DETECT_ROTATION_
#define DETECT_ROTATION_

#include "util.h"
#include "graphics.h"

#define M_PI 3.14159265358979323846

float DegToRadians(float deg);
float RadToDegrees(float rad);
int HoughLineDetection(SDL_Surface* image, unsigned int* pixels);
void CenteredRotation(SDL_Surface* image, float angle, unsigned int* pixels);
void Rotation(SDL_Surface* image, unsigned int* pixels);

#endif
